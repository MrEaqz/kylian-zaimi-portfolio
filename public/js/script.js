window.onload = () => {
  setTimeout(() => {
      let el = document.getElementById('loader');
      el.style.backdropFilter = "blur(0px)"
      el.style.backgroundColor = "rgba(0, 0, 0, 0.0)"
    setTimeout(() => {
      let el = document.getElementById('loader');
      el.style.display = 'none';
    }, 500)

  }, 1000);
}

window.requestAnimFrame = (function(){
    return  window.requestAnimationFrame       ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame    ||
            function( callback ){
              window.setTimeout(callback, 1000 / 60);
            };
  })();
  function scrollToY(o, n) {
    function calculate() {
        t += 1 / 400; // time to scroll
        const n = t / c,
            l = i(n);
        1 > n ? (requestAnimFrame(calculate), window.scrollTo(0, r + (o - r) * l)) : window.scrollTo(0, o);
    }
        r = window.scrollY,
        o = o || 0,
        n = n || 2e3,
        t = 0,
        c = Math.max(0.1, Math.min(Math.abs(r - o) / n, 0.8)),
        i = function (o) {
            return -0.5 * (Math.cos(Math.PI * o) - 1);
        };
        calculate();
}

  !function() {
    let el;
    const _scrollToY = function(event) {
      event.preventDefault ? event.preventDefault() : (event.returnValue = false);
      let el = this;
      let name = (el.getAttribute("href") || el.href).slice(1);
      el = document.getElementById(name);
      if (typeof el === 'object' && el.offsetTop) {
        scrollToY(el.offsetTop, 1500); 
        return false;
      }
    }
    for (let a=document.querySelectorAll('a[href^="#section-"]'), i=0; i<a.length; i++) {
      el=a[i];
      if (el.addEventListener){
        el.addEventListener('click', _scrollToY, false);
      } else if (el.attachEvent) {
        el.attachEvent('onclick', _scrollToY);
      }
    }
  }();


mybutton = document.getElementById("scrolltop");

window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 400 || document.documentElement.scrollTop > 400) {
    mybutton.style.opacity = "1";
    mybutton.style.cursor = "pointer";
    mybutton.href = "#section-header";
  } else {
    mybutton.style.opacity = "0";
    mybutton.style.cursor = "default";
    mybutton.style.zindex = "-9999";
    mybutton.href = "#";
  }
}
