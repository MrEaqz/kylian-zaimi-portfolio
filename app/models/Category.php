<?php

use Phalcon\Mvc\Model;

class Category extends Model
{

    public $id;
    public $name;
    public $active;

    public function initialize()
    {
        $this->setSource("category");
    }

}
