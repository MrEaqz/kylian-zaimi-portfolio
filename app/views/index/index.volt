<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="description" content="Développeur web freelance spécialisé dans la création de site internet ainsi que d'identitée numérique"/>
    <meta name="robots" content="follow"/>
    <meta name="author" content="ZAIMI Kylian"/>
    <meta name="copyright" content="ZAIMI Kylian"/>
    <meta property="og:title" content="Zaïmi Kylian">
    <meta property="og:description" content="Développeur web freelance spécialisé dans la création de site internet ainsi que d'identitée numérique.">
    <meta property="og:image" content="https://kylian-zaimi.fr/img/other/orange.png">
    <meta property="og:image:type" content="image/png" />
    <meta property="og:image:width" content="200" />
    <meta property="og:image:height" content="200" />
    <meta property="og:url" content="https://kylian-zaimi.fr">
    <title>Kylian Zaimi | Développeur web freelance spécialisé dans la création de site internet clé en main basé à Montpellier.</title>

    <!-- Stylesheet -->
    <link rel="stylesheet" href="css/main.css?2">

    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
</head>
<body>
    <div class="loader" id="loader"></div>

    <a href="#section-header" class="scroll-top" id="scrolltop">
        <img src="img/icon/arrow-up.svg"/>
    </a>

    <section class="header" id="section-header">
        <div class="header-row">
            
            <div class="col-1">
                <img alt="Some good image" class="main-logo" src="img/logo.png" height="77px">
            </div>

            <div class="col-2">
                <a href="#section-main-content" class="nav-item nav-toggle">Accueil</a>
                <a href="#section-works" class="nav-item">Projets</a>
                <a href="#section-about" class="nav-item">A propos</a>
                <a href="#section-contact" class="nav-item">Contact</a>
            </div>

            <div class="col-3">
                <form action="#" method="post">
                    <input type="hidden" name="type" value="resume">
                    <button class="special-fancy-btn">Télécharger mon CV</button>
                </form>
            </div>

            <img alt="Some good image" class="absolute-gardient-2" src="img/layout/gradient2.png"/>
        </div>
    </section>

    <section class="main-content" id="section-main-content">

        <div class="left">
            <div class="title">
                <h1>Salut ! Moi c'est Kylian 
                    <img alt="Some good image" class="arok" src="img/other/armoji-ok.gif"/>
                </h1>
                
            </div>
            <div class="desc">
                <span class="first-job">Full stack web</span>
                <span class="stack"> (PHP/HTML/CSS/JS)</span><br>
                <span class="second-job">UI Designer </span>
                <span class="and"> et </span>
                <span class="third-job">Freelance</span>
            </div>
            <div class="action">
                <a href="#section-contact" class="call-to-action">Say hi  👋</a>
                <a href="https://gitlab.com/MrEaqz" target="_blank"> <img alt="Some good image" id="gitlab-redirect" class="social" src="img/icon/gitlab-colored.svg"/></a>
                <a href="https://www.linkedin.com/in/kylian-za%C3%AFmi-78b75312b/" target="_blank"> <img alt="Some good image" id="linkedin-redirect" class="social" src="img/icon/linkedin-colored.svg"/></a>
                <a href="https://discordapp.com/users/198905794593292299/" target="_blank"> <img alt="Some good image" id="discord-redirect" class="social" src="img/icon/discord-colored.svg" height="33px"/></a>
            </div>
        </div>

        <div class="right-img">
            <img alt="Some good image" class="argood" src="img/other/armoji-good.gif"/>
            <img alt="Some good image" class="absolute-gardient-1" src="img/layout/Gradient.png"/>
        </div>

        <img alt="Some good image" src="img/layout/scroll-down.gif" alt="" class="scroll-down">
    </section>
    
    <section class="works" id="section-works">
        <img alt="Some good image" class="absolute-gardient-3" src="img/layout/gradient4.png"/>
        <div class="title">
            <h1>Expérience</h1>
            <h2>Voici les projets que j’ai eu l’occasion de développer et le stack technique utilisé.</h2>
        </div>
        <div class="projects">
            <div class="card transparent">
                <img alt="Some good image" class="later" src="img/layout/Later.png">
            </div>

            <div class="card">
                <img alt="Some good image" class="project-logo" src="img/icon/logo-izp.png" height="50px"/>
                <h1 class="project-date">Septembre 2021 - <b>*CDI*</b></h1>
                <h2 class="project-name">InzPok</h2>
                <h3 class="project-info">InzPok est la 1ere solution de contrôle parental qui permet aux enfants de découvrir la e-consommation selon les principes éducatifs de leur famille.  <a class="see-more" target="_blank" href="https://www.inzpok.com/"> Voir plus</a></h3>
                <div class="separator"></div>
                <div class="card-footer">
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/python_colored.svg?3"/>
                        <span class="tooltip">Python</span>
                    </div>
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/django.svg?3"/>
                        <span class="tooltip">Framework Python Django</span>
                    </div>
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/elasticsearch.svg?3"/>
                        <span class="tooltip">Elasticsearch</span>
                    </div>
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/git_colored.svg"/>
                        <span class="tooltip">GIT - Gitlab</span>
                    </div>
                </div>
            </div>
            <div class="card">
                <img alt="Some good image" class="project-logo" src="img/icon/logo-gssa.png" height="50px"/>
                <h1 class="project-date">Août 2021</h1>
                <h2 class="project-name">Gallia Sport Saint Aunes </h2>
                <h3 class="project-info">Gallia Sport Saint Aunes (GSSA) est un club de foot local en périphérie de Montpellier. J'ai eu pour mission de développer de leurs site vitrine.</h3>
                <div class="separator"></div>
                <div class="card-footer">
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/css_colored.svg"/>
                        <span class="tooltip">CSS</span>
                    </div>
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/html_colored.svg"/>
                        <span class="tooltip">HTML</span>
                    </div>
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/git_colored.svg"/>
                        <span class="tooltip">GIT - Gitlab</span>
                    </div>
                </div>
            </div>
            
            <div class="card">
                <img alt="Some good image" class="project-logo" src="img/icon/logo-candleaf.svg" height="50px"/>
                <h1 class="project-date">Juillet 2021</h1>
                <h2 class="project-name">Candleaf</h2>
                <h3 class="project-info">Candleaf est un site de vente en ligne de bougie (projet d'exemple), j'ai créé et intégré la maquette, et la partie back-end est encore en cours de développement. <a class="see-more" target="_blank" href="https://gitlab.com/MrEaqz/candleaf"> Voir plus</a></h3>
                <div class="separator"></div>
                <div class="card-footer">
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/phalcon_colored.svg"/>
                        <span class="tooltip">Framework PHP - Phalcon</span>
                    </div>
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/css_colored.svg"/>
                        <span class="tooltip">CSS</span>
                    </div>
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/html_colored.svg"/>
                        <span class="tooltip">HTML</span>
                    </div>
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/php_colored.svg" style="vertical-align: 5px"/>
                        <span class="tooltip">PHP</span>
                    </div>
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/git_colored.svg"/>
                        <span class="tooltip">GIT - Gitlab</span>
                    </div>
                </div>
            </div>

            <div class="card">
                <img alt="Some good image" class="project-logo" src="img/icon/logo-vicelife.png"/>
                <h1 class="project-date">Juin 2021</h1>
                <h2 class="project-name">ViceLife</h2>
                <h3 class="project-info">Vicelife est une communauté "gaming", j'ai eu pour mission de développer un site vitrine ainsi qu'un backoffice connecté à leurs API . <a class="see-more" target="_blank" href="http://viceliferp.fr/">Voir plus</a></h3>
                <div class="separator"></div>
                <div class="card-footer">
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/phalcon_colored.svg"/>
                        <span class="tooltip">Framework PHP - Phalcon</span>
                    </div>
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/js_colored.svg"/>
                        <span class="tooltip">Javascript</span>
                    </div>
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/css_colored.svg"/>
                        <span class="tooltip">CSS</span>
                    </div>
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/html_colored.svg"/>
                        <span class="tooltip">HTML</span>
                    </div>
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/php_colored.svg" style="vertical-align: 5px"/>
                        <span class="tooltip">PHP</span>
                    </div>
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/git_colored.svg"/>
                        <span class="tooltip">GIT - Gitlab</span>
                    </div>
                </div>
            </div>
            
            <div class="card">
                <img alt="Some good image" class="project-logo" src="img/icon/logo-viavino.png"/>
                <h1 class="project-date">Mai 2021</h1>
                <h2 class="project-name">Viavino - Le Restaurant</h2>
                <h3 class="project-info">Viavino est un pôle oenotouristique à Saint Christol (périphérie de Montpellier). J'ai eu pour mission de développer le site vitrine de leurs restaurant.</h3>
                <div class="separator"></div>
                <div class="card-footer">
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/css_colored.svg"/>
                        <span class="tooltip">CSS</span>
                    </div>
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/html_colored.svg"/>
                        <span class="tooltip">HTML</span>
                    </div>
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/git_colored.svg"/>
                        <span class="tooltip">GIT - Gitlab</span>
                    </div>
                </div>
            </div>
            
            <div class="card">
                <img alt="Some good image" class="project-logo" src="img/icon/control-logo.png"/>
                <h1 class="project-date">Avril 2020 jusqu'à aujourd'hui</h1>
                <h2 class="project-name">Control Gaming</h2>
                <h3 class="project-info">Control est un serveur de jeu roleplay. J'ai à ma charge la gestion de la communauté, du développement web ainsi que du développement des interfaces en jeu. <a class="see-more" target="_blank" href="https://control-roleplay.com/">Voir plus</a></h3>
                <div class="separator"></div>
                <div class="card-footer">
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/phalcon_colored.svg"/>
                        <span class="tooltip">Framework PHP - Phalcon</span>
                    </div>
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/node_colored.svg"/>
                        <span class="tooltip">NodeJS</span>
                    </div>
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/js_colored.svg"/>
                        <span class="tooltip">Javascript</span>
                    </div>
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/css_colored.svg"/>
                        <span class="tooltip">CSS</span>
                    </div>
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/html_colored.svg"/>
                        <span class="tooltip">HTML</span>
                    </div>
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/php_colored.svg" style="vertical-align: 5px"/>
                        <span class="tooltip">PHP</span>
                    </div>
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/git_colored.svg"/>
                        <span class="tooltip">GIT - Gitlab</span>
                    </div>
                </div>
            </div>
            
            <div class="card">
                <img alt="Some good image" class="project-logo" src="img/icon/logo-ventura.png"/>
                <h1 class="project-date">Juin 2019 jusqu'à Mars 2020</h1>
                <h2 class="project-name">Ventura Community</h2>
                <h3 class="project-info">Ventura est une communauté rassemblant les pationnés de jeu vidéo. J'avais à ma charge la gestion de l'équipe de développement ainsi que la partie web.</h3>
                <div class="separator"></div>
                <div class="card-footer">
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/phalcon_colored.svg"/>
                        <span class="tooltip">Framework PHP - Phalcon</span>
                    </div>
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/node_colored.svg"/>
                        <span class="tooltip">NodeJS</span>
                    </div>
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/js_colored.svg"/>
                        <span class="tooltip">Javascript</span>
                    </div>
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/css_colored.svg"/>
                        <span class="tooltip">CSS</span>
                    </div>
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/html_colored.svg"/>
                        <span class="tooltip">HTML</span>
                    </div>
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/php_colored.svg" style="vertical-align: 5px"/>
                        <span class="tooltip">PHP</span>
                    </div>
                    <div class="tooltip-container">
                        <img alt="Some good image" class="project-stack" src="img/icon/git_colored.svg"/>
                        <span class="tooltip">GIT - Gitlab</span>
                    </div>
                </div>
            </div>
            
        </div>
    </section>

    <section class="about" id="section-about">
        <img alt="Some good image" class="absolute-gardient-5" src="img/layout/gradient5.png"/>
        <img alt="Some good image" class="armoji" src="img/other/armoji-dancing.gif"/>
        <div class="title">
            <h1>A propos</h1>
            <h2>Voulez-vous en savoir plus sur moi ?</h2>
        </div>
        <p>
            Tout a commencé par le big bang [...] mais là n'est pas le sujet.
            <br><br>
            Je suis Kylian, développeur web autodidacte basé en périphérie de Montpellier (34000) et <br>guidé par ma passion et ma motivation.
            <br><br>
            Mais comme vous le savez, ce n’est pas la passion qui créé l’expérience, c’est pourquoi j’ai lié ma passion pour <br>le développement web ainsi que pour les jeux vidéo en entreprenant des projets communautaires de grande envergure.
            <br><br>
            Actuellement en freelance depuis mars 2021 j’écoute toutes les propositions d’emploi / missions.
            <br>
            Si vous voulez me faire part de vos projets, rendez-vous dans la section "Contact", où je serais heureux d’échanger avec vous :-)
        </p>
    </section>

    <section class="contact" id="section-contact">
        <img alt="Some good image" class="absolute-gardient-6" src="img/layout/gradient6.png"/>
        <img alt="Some good image" class="armoji" src="img/other/armoji-yo.png"/>
        <div class="title">
            <h1>Contact</h1>
            <h2>Intéressé par mon profil ? Envie d'échanger ?</h2>
        </div>
        <form class="form" action="#" method="post">
            <input type="hidden" name="type" value="contact">
            <div class="bg"></div>
            <div class="col-1">
                <input class="contact-input" name="name" type="text" placeholder="Votre nom">
                <input class="contact-input" name="email" type="text" placeholder="Votre email" style="margin-left: 21px;">
            </div>
            <div class="col-2">
                <textarea id="contact-textarea" name="message" cols="70" rows="5" placeholder="Message"></textarea>
            </div>
            <div class="col-3">
                <input type="hidden" name="captcha" value="{{ captcha / 2 }}">
                <input class="contact-input" name="math" type="text" placeholder="{{ captcha / 2 }} + {{ captcha / 2 }} = ?">
                <button class="fancy-submit-button" type="submit" style="margin-left: 21px;">Envoyer</button>
            </div>
        </form>
    </section>

    <section class="wip" id="section-wip">
        <img alt="Some good image" class="absolute-gardient-4" src="img/layout/gradient4.png"/>
        <h1>C'est pour bientôt....</h1>
        <h2>*Je plaisante pas :-)*</h2>
        <img alt="Some good image" class="soon" src="img/layout/soon.png"/>
    </section>

    <section class="footer">
        <span class="credit">Intégré, designé, développé avec ❤️ par MrEaqz (ZAIMI Kylian) </span>
    </section>

    <script type="text/javascript" src="js/script.js"></script>
</body>
</html>