<?php
class IndexController extends ControllerBase
{
    public function initialize()
    {
        parent::initialize();
    }

    public function generateCaptcha() 
    {
        for ($i=0; $i < 70; $i++) { 
            $captcha = rand(4, 30);
            if (gettype($captcha/2) === 'integer' && $captcha) {
                return $captcha;
            } else {
                $i--;
            }
        }
    }

    public function indexAction()
    {
        $this->view->captcha = $this->generateCaptcha();

            if ($this->request->isPost()) {
                switch ($this->request->getPost("type", "striptags")) {
                    case 'contact':
                        $name = $this->request->getPost("name", "striptags");
                        $email = $this->request->getPost("email", "striptags");
                        $message = $this->request->getPost("message", "striptags");
                        $math = $this->request->getPost("math", "striptags");
                        $captcha = $this->request->getPost("captcha", "striptags");
                        if ($math && $captcha && ($captcha * 2) == $math) {
                            // GOOD CAPTCHA
                            if ($name && $email && $message) {
                                $finalMessage = "Nom: $name\n\nMail: $email\n\nMessage :\n$message";
                                $dataToPush = array(
                                    "from"      => "contact@kylian-zaimi.fr",
                                    "to"        => "ky.zaimi@gmail.com",
                                    "subject"   => "En provenance de www.kylian-zaimi.fr V2",
                                    "content"   => $finalMessage,
                                );
                                $authpass = $this->config->smtp->authpass;
                                $authuser = $this->config->smtp->authuser;
                                $this->callApi("POST", "https://api.turbo-smtp.com/api/mail/send?authuser=$authuser&authpass=$authpass", $dataToPush);
        
                                print_r("Message envoyé avec succès."); die;
                            } else {
                                print_r("Tout les champs ne sont pas correctement remplis."); die;
                            }
                        } else {
                            print_r("Vous n'avez pas réussi votre question de sécurité."); die;
                            // BAD CAPTCHA
                        }
                        break;
                    case 'resume':
                        $this->response->redirect('CV.pdf');
                        break;
                }
            }


        // $langSession = $this->session->get('lang');

        // if (!$langSession) {
        //     $translations = (object)$this->getTranslations($this->getBestLanguage());
        //     $translations = (object)$this->getTranslations("fr");

        //     $this->view->translations = $translations;
        // } else {
        //     $translations = (object)$this->getTranslations($langSession['userlang']);
        //     $this->view->translations = $translations;
        // }
        // $translations = (object)$this->getTranslations();
        // $this->view->translations = $translations;
        // $this->tag->setTitle($translations->title);

        // $selectedLanguage = $this->request->getQuery("language", "striptags");
        // if ($selectedLanguage) { 
        //     switch ($selectedLanguage) {
        //         case 'fr':
        //             $this->session->set(
        //                 'lang',
        //                 [
        //                     'userlang'      => "fr",
        //                 ]
        //             );
        //             $this->response->redirect("index");
        //             break;
        //         case 'en':
        //             $this->session->set(
        //                 'lang',
        //                 [
        //                     'userlang'      => "en",
        //                 ]
        //             );
        //             $this->response->redirect("index");
        //             break;
        //         default:
        //             $this->session->set(
        //                 'lang',
        //                 [
        //                     'userlang'      => "fr",
        //                 ]
        //             );
        //             $this->response->redirect("index");
        //             break;
        //     }
        // }

        // if ($this->request->isPost()) {
        //     $name = $this->request->getPost("name", "striptags");
        //     $email = $this->request->getPost("email", "striptags");
        //     $phone = $this->request->getPost("phone", "striptags");
        //     $subject = $this->request->getPost("subject", "striptags");
        //     $message = $this->request->getPost("message", "striptags");
            
        //     if ($name && $email && $phone && $subject && $message) {
        //         $finalMessage = "Nom: $name\nTel: $phone\nMail: $email\n\nMessage :\n$message";
        //         $dataToPush = array(
        //             "from"      => "contact@kylian-zaimi.fr",
        //             "to"        => "ky.zaimi@gmail.com",
        //             "subject"   => "En provenance de www.kylian-zaimi.fr - " . $subject,
        //             "content"   => $finalMessage,
        //         );
        //         $authpass = $this->config->smtp->authpass;
        //         $authuser = $this->config->smtp->authuser;
        //         $this->callApi("POST", "https://api.turbo-smtp.com/api/mail/send?authuser=$authuser&authpass=$authpass", $dataToPush);

        //     } else {

        //     }
        // }

    }
}

