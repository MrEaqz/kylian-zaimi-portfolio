<?php
declare(strict_types=1);

class ErrorsController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Oops!');
        $this->view->setTemplateAfter('errors');
    }

    public function show404Action(): void
    {
        $this->response->redirect('index');
    }

    public function show401Action(): void
    {
        $this->response->redirect('index');
    }

    public function show500Action(): void
    {
        $this->response->redirect('index');
    }

    public function show503Action(): void
    {
        $this->response->redirect('index');
    }
}

