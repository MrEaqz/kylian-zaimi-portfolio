<?php

use Phalcon\Mvc\Controller;
use Phalcon\Filter;
use Phalcon\Http\Response;

class ControllerBase extends Controller
{
    protected function initialize()
    {
        $this->tag->prependTitle('Kylian Zaimi | ');

        $this->view->setTemplateAfter('main');
    }

    /**
     * @return string (get client language)
     */
    public function getBestLanguage() 
    {
        switch ($this->request->getBestLanguage()) {
            case 'en-US':
                $bestLanguage = "en";
                break;
            case 'en':
                $bestLanguage = "en";
                break;
            case 'fr-FR':
                $bestLanguage = "fr";
                break;
            case 'fr':
                $bestLanguage = "fr";
                break;
            
            default:
            $bestLanguage = "fr";
                break;
        }

        return $bestLanguage ? $bestLanguage : "fr";
    }

    /**
     * @param string $lang
     * 
     * @return array (with all translations)
     */
    public function getTranslations(string $lang = "fr") 
    {
        $translationFile = $_SERVER['DOCUMENT_ROOT'] . '/app/translation/'. $lang .'.php';

        if (file_exists($translationFile)) {
            require $translationFile;
            return (object)$messages;
        } else {
            $filePath = $_SERVER['DOCUMENT_ROOT'] . 'app/translation/fr.php';
            require $filePath;
            return (object)$messages;
        }
    }

    /**
     * @param string $params
     * 
     * @return void (refresh the client page)
     */
    public function refreshPage($params = null)
    {
        $actualRoad = $this->dispatcher->getControllerName() . "/" . $this->dispatcher->getActionName();
        if ($params) {
            $actualRoad = $actualRoad . $params;
        }
        $this->response->redirect($actualRoad);
    }

        /**
     * @param enum GET|POST|PUT|DELETE
     * @param string $action (URL)
     * @param string $params
     */
    public function callApi($methode, $vanillaUrl, $params = null)
    {
        switch ($methode) {

            case "GET":
                if ($params) {
                    $vanillaUrl = $vanillaUrl . $params;
                }
                $curl = curl_init($vanillaUrl);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                $curl_response = curl_exec($curl);
                $decoded = json_decode($curl_response);

                curl_close($curl);
                return !empty($decoded) && $decoded->code == 200 ? $decoded : 504;
            break;
            
            case "POST":
                $curl = curl_init($vanillaUrl);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($params));
                curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                ));
                $curl_response = curl_exec($curl);
                $decoded = json_decode($curl_response);
                
                curl_close($curl);
                return !empty($decoded) && $decoded->code == 200 ? $decoded : 504;
            break;

            case "PUT":
                $curl = curl_init($vanillaUrl);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($params));
                curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                ));
                $curl_response = curl_exec($curl);
                $decoded = json_decode($curl_response);                
                
                curl_close($curl);
                return !empty($decoded) && $decoded->code == 200 ? $decoded : 504;
            break;

            case "DELETE":
            break;
        }
    }
}
